package modele;

import java.util.Hashtable;
import java.util.Objects;
import java.util.Vector;

public class Societe {

    //attribut d'instance
    private String nom;

    //association Societe 1 <--> * Client
    private Vector<Client> ens = new Vector<>();

    //getter setter

    public String getNom() {
        return nom;
    }

    public Vector<Client> getEns(){
        return ens;
    }

    private void setNom(String nom) {
        this.nom = (nom==null? "SOCIETE": nom.toUpperCase());
    }



    //constructeur

    public Societe(String nom) {
        this.setNom(nom);
    }

    protected Societe(){
        this(null);
    }

    //equals hashcode basées sur la methode rigide et l'attribut nom

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Societe societe = (Societe) o;
        return nom.equals(societe.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

    //to string

    @Override
    public String toString() {
        String str = "Societe{" +
                "nom='" + nom + "\n Listye des clients";
        for (Client c1: this.ens){
        str = str + "\n\t" + c1.toString();
        }
        /*ens.forEach(c2 ->
                str = str + "\n\t" + c2.toString()
                );*/
        return str + "}";
    }



    //Methode metier
    //ajout d'un client
    public void ajouterClient(Client c){
        //controle 1 existance du client
        if (c==null) return;

        //controle 2 le client doit etre inconnu de la societe
        if (this.ens.contains(c)) return;

        //action ajout dans le taleau dynamique
        this.ens.addElement(c);
        //System.out.println(ens);

        c.setSociete(this.nom);
    }

    public static void main(String[] args) {
        Societe popschool = new Societe("popschool");
        Client mic = new Client("Lefebvre", "Michael", null);
        Client marc = new Client("Bouchez", "Marc", null);
        popschool.ajouterClient(mic);
        System.out.println(mic);
        System.out.println(marc);
        Societe pol = new Societe("pole-emploi");
        popschool.ajouterClient(marc);
        System.out.println(marc);
        System.out.println(popschool);

    }

}

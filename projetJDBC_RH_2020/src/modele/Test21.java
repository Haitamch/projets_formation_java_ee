package modele;

import java.sql.*;

public class Test21 {
    private static String SQLajoutEmploye =
            " insert into employee (empno, ename, sal, deptno) "
                    +" values(6789, 'BELIVIER', 500, 10)";
    private static String SQLajoutEmploye2 =
            " insert into employee (empno, ename, sal, deptno) "
                    +" values(6790, 'TOLIVER', 500, 10)";
    private static String SQLajoutEmploye3 =
            " insert into employee (empno, ename, sal, deptno) "
                    +" values(6791, 'MARCOLIV', 500, 10)";

    private static String SQLmodificationEmploye =
            "UPDATE employee e, "
            + " (select count(distinct sal) id from employee where sal < e.sal) as test "
                    +"    set sal = sal *1.1 "
            +"    where 2>= test.id ";

    // e1 est solution SSI on ne peut trouver que 0, 1 ou 2 salaire(s).
    // inférieur(s) au sien (e1.sal)

    public static void main(String[] args)
            throws  SQLException, ClassNotFoundException, java.io.IOException
    {
        // Liaison avec la Base de données
        String DRIVER = "com.mysql.jdbc.Driver";
        String URL = "jdbc:mysql://localhost:3306/employes";
        String USER = "michael";
        String PW = "mdppopmichael";

        // Charge le driver
        Class.forName(DRIVER);
        // Un exemple ˆ ne pas suivre : mettre le mot de passe en dur
        Connection conn = null;
        Statement stmt = null;
        try {
            // etablir la connexion
            conn = DriverManager.getConnection(URL, USER, PW);
            // gestion de la transaction par l'application
            conn.setAutoCommit(false);

            stmt = conn.createStatement();

            // insertion en BD
            int nbLignes = stmt.executeUpdate(SQLajoutEmploye);
            System.out.println(nbLignes);

            // insertion en BD
            int nbLignes2 = stmt.executeUpdate(SQLajoutEmploye2);
            System.out.println(nbLignes2);

            // insertion en BD
            int nbLignes3 = stmt.executeUpdate(SQLajoutEmploye3);
            System.out.println(nbLignes3);

            // modification de la BD
            nbLignes = stmt.executeUpdate(SQLmodificationEmploye);

            // validation de la transaction
            conn.commit();
            System.out.println(nbLignes);
        }catch (SQLException ex){
            ex.printStackTrace();
            conn.rollback();
        }
        finally {
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
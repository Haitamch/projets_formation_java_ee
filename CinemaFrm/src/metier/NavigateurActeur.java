package metier;


import dto.ActeurDTO;
import dto.ConnectCinema;


import java.sql.ResultSet;
import java.sql.*;


public class NavigateurActeur {

    //Necessaire pour la question 2
    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet acteurRS;

    private int nbActeur;

    public int getNbActeur() {
        return nbActeur;
    }

    public int position()throws SQLException{
        return acteurRS.getRow();
    }

    public NavigateurActeur(){
        try{
            ouvrirConection();
            acteurRS.last();
            nbActeur = acteurRS.getRow();

            acteurRS.first();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private final static String SQLselectActeur =
                "select nacteur, a.nom as nom, prenom, naissance, p.nom as nationalite, nbreFilms "
            +   " FROM ACTEUR a "
            +   " INNER JOIN PAYS p "
            +   " ON nationalite = npays"
            +   " order by a.nom, prenom"
            ;

    public void ouvrirConection() throws SQLException {
        //etablir la connection
        this.conn = ConnectCinema.getInstance();

        this.stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
        this.acteurRS = stmt.executeQuery(SQLselectActeur);
    }

    public void fermerConnection() throws SQLException {
        if(this.stmt != null){
            stmt.close();
        }
        if(this.conn != null){
            conn.close();
        }
    }
    public ActeurDTO getFirstActeur() throws SQLException{
        if (this.acteurRS==null)
            return null;

        acteurRS.first();

        int nacteur = acteurRS.getInt(1);
        String nom = acteurRS.getString(2);
        String prenom = acteurRS.getString(3);
        java.util.Date naissance = acteurRS.getDate(4);
        String nationalite = acteurRS.getString(5);
        int nbreFilms = acteurRS.getInt(6);

        return new ActeurDTO(nacteur,nom,prenom,naissance,nationalite,nbreFilms);
    }

    public ActeurDTO getNextActeur() throws SQLException{
        if (this.acteurRS==null)
            return null;
        if (this.acteurRS.isLast())
            return null;

        acteurRS.next();

        int nacteur = acteurRS.getInt(1);
        String nom = acteurRS.getString(2);
        String prenom = acteurRS.getString(3);
        java.util.Date naissance = acteurRS.getDate(4);
        String nationalite = acteurRS.getString(5);
        int nbreFilms = acteurRS.getInt(6);

        return new ActeurDTO(nacteur,nom,prenom,naissance,nationalite,nbreFilms);
    }

    public ActeurDTO getPreviousActeur() throws SQLException{
        if (this.acteurRS==null)
            return null;
        if (this.acteurRS.isFirst())
            return null;

            acteurRS.previous();

        int nacteur = acteurRS.getInt(1);
        String nom = acteurRS.getString(2);
        String prenom = acteurRS.getString(3);
        java.util.Date naissance = acteurRS.getDate(4);
        String nationalite = acteurRS.getString(5);
        int nbreFilms = acteurRS.getInt(6);

        return new ActeurDTO(nacteur,nom,prenom,naissance,nationalite,nbreFilms);
    }

    public ActeurDTO getLastActeur() throws SQLException{
        if (this.acteurRS==null)
            return null;

        acteurRS.last();

        int nacteur = acteurRS.getInt(1);
        String nom = acteurRS.getString(2);
        String prenom = acteurRS.getString(3);
        java.util.Date naissance = acteurRS.getDate(4);
        String nationalite = acteurRS.getString(5);
        int nbreFilms = acteurRS.getInt(6);

        return new ActeurDTO(nacteur,nom,prenom,naissance,nationalite,nbreFilms);
    }

    public static void main(String[] args) throws ClassNotFoundException, SQLException{
        NavigateurActeur cine = new NavigateurActeur();
        cine.ouvrirConection();

        ActeurDTO acteur = cine.getFirstActeur();
        while (acteur != null){
            System.out.println(acteur);

            acteur = cine.getNextActeur();
        }
        cine.fermerConnection();
    }

}

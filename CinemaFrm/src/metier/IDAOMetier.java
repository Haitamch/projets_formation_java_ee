package metier;

import dto.ActeurDTO;
import dto.FilmDTO;
import dto.GenreDTO;

import java.util.*;

public interface IDAOMetier {

    List<GenreDTO> ensGenres();

    long nbreFilmDuGenre(int numGenre);

    List<FilmDTO> ensFilmsDuGenre(int numGenre);

    FilmDTO infoRealisateurEtActeur(int numFilm);

    List<ActeurDTO> ensActeurs();

    List<FilmDTO> ensTitreDunActeur(int numActeur);


}

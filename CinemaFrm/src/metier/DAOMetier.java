package metier;

import dto.ActeurDTO;
import dto.ConnectCinema;
import dto.FilmDTO;
import dto.GenreDTO;
import javafx.collections.ArrayChangeListener;

import java.lang.reflect.Array;
import java.sql.*;
import java.util.*;

public class DAOMetier implements IDAOMetier{

    private final static String SQLfindALLGenres =
            "SELECT ngenre, nature  " +
                    " FROM GENRE ORDER BY nature";

    private final static String SQLfindNbFilmsByGenre =
            "SELECT COUNT(*) as NB " +
                    " from FILM " +
                    " where ngenre = ?";

    private final static String SQLfindAllFilmByGenre =
            "SELECT nfilm , titre, realisateur as NOM_REALISATEUR, CONCAT(nom, ' ',prenom) as NOM_ACTEUR " +
                    " from FILM f " +
                    " INNER JOIN ACTEUR ON nacteur = nacteurPrincipal " +
                    "where f.ngenre = ? " +
                    "ORDER BY titre";

    private final static String SQLfindFilmById =
            "SELECT nfilm , titre, realisateur as NOM_REALISATEUR, nom as NOM_ACTEUR " +
                    " from FILM f " +
                    " INNER JOIN ACTEUR ON nacteur = nacteurPrincipal " +
                    " where f.nfilm = ? " +
                    "ORDER BY titre";

    //---------------------------------------------------------------------------------------------
    @Override
    public List<GenreDTO> ensGenres() {
        List<GenreDTO> liste = new ArrayList<>();

        try{
            Statement instr = ConnectCinema.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLfindALLGenres);

            while (rs.next()){
                int ngenre = rs.getInt(1);
                String nature = rs.getString("nature");

                liste.add(new GenreDTO(ngenre, nature));
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return liste;
    }

    @Override
    public long nbreFilmDuGenre(int numGenre) {
        try{
            PreparedStatement ps =
                    ConnectCinema.getInstance().prepareStatement(SQLfindNbFilmsByGenre);

            ps.setInt(1,numGenre);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                long nb = rs.getLong("NB");

                return nb;
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return -1L;
    }

    @Override
    public List<FilmDTO> ensFilmsDuGenre(int numGenre) {
        List<FilmDTO> liste = new ArrayList<>();
        try{
            PreparedStatement ps = ConnectCinema.getInstance().prepareStatement(SQLfindAllFilmByGenre);

            ps.setInt(1,numGenre);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nomReal = rs.getString(3); //3 correspond a la colonne 3
                String nomActeur = rs.getString(4); //4 correspond a la colonne 4

                FilmDTO f = new FilmDTO(nfilm, titre, nomReal, nomActeur);
                liste.add(f);
            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return liste;
    }

    @Override
    public FilmDTO infoRealisateurEtActeur(int numFilm) {
        FilmDTO f = null;
        try{
            PreparedStatement ps = ConnectCinema.getInstance().prepareStatement(SQLfindFilmById);
            ps.setInt(1,numFilm);
            ResultSet rs = ps.executeQuery();

            if(rs.next()){
                int nfilm = rs.getInt("nfilm");
                String titre = rs.getString("titre");
                String nomReal = rs.getString(3);
                String nomActeur = rs.getString(4);

                f = new FilmDTO(nfilm, titre,nomReal,nomActeur);
            }

        }catch(SQLException ex){
            ex.printStackTrace();
        }
        return f;
    }

    // ---------------------------------------S I N G L E T O N   P R É C O C E -----------------------
    private static  DAOMetier INSTANCE = new DAOMetier();
    private DAOMetier(){}
    public static IDAOMetier getInstance(){
        return INSTANCE;
    }

    //------------------------------------------------------------------------------
    @Override
    public List<ActeurDTO> ensActeurs() {
        List<ActeurDTO> liste = new ArrayList<>();
       /*try{
            PreparedStatement ps = ConnectCinema.getInstance().prepareStatement();

        }catch(SQLException ex){
            ex.printStackTrace();
        }*/
        return liste;
    }

    @Override
    public List<FilmDTO> ensTitreDunActeur(int numActeur) {
        return null;
    }

    public static void main(String[] args) {
        DAOMetier dao = new DAOMetier();
        System.out.println("-------------------liste genre-------------------");
        System.out.println(dao.ensGenres());
        System.out.println("---------------nombre de film-------------------------");
        System.out.println("Categorie 2 a : " + dao.nbreFilmDuGenre(2) + " film(s)");
        System.out.println("----------------------liste film du genre-----------------------");
        System.out.println(dao.ensFilmsDuGenre(2));
        System.out.println("--------------------------info real acteur----------------------------");
        FilmDTO f = dao.infoRealisateurEtActeur(3);
        System.out.println(f.getTitre() +"\n"
                            +   f.getNomRealisateur() + "\n"
                            +   f.getNomActeur());
    }
}

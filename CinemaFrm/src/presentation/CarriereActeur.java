package presentation;

import dto.ActeurDTO;
import metier.NavigateurActeur;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CarriereActeur {
    private NavigateurActeur cinema;
    private ActeurDTO acteur = null;
    private int nacteur = 0;

    private JButton button1;
    private JPanel rootPanel;
    private JButton button2;
    private JProgressBar progressBar1;
    private JButton button3;
    private JButton button4;
    private JTextField textField1;
    private JTextField textField2;
    private JLabel prenomLabel;
    private JLabel nomLabel;
    private JPanel test;
    private JPanel panel2;
    private Color sombre = new Color(89,91,93);
    private Color blue = new Color(53,146,196);

    public CarriereActeur() throws Exception {
        init();
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    acteur = cinema.getNextActeur();
                }catch (Exception ex) {
                    System.err.println(ex.getMessage());
                }
                if(acteur != null) {
                    infoActeurBD();
                    progressBar1.setValue(acteur.getNacteur());
                    button2.setEnabled(true);
                }else{
                    button3.setEnabled(false);
                }
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    acteur = cinema.getPreviousActeur();
                }catch (Exception ex) {
                    System.err.println(ex.getMessage());
                }
                if(acteur != null) {
                    infoActeurBD();
                    button3.setEnabled(true);
                    progressBar1.setValue(acteur.getNacteur());
                }else{
                    button2.setEnabled(false);
                }
            }
        });

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    acteur = cinema.getFirstActeur();
                    button2.setEnabled(false);
                }catch (Exception ex) {
                    System.err.println(ex.getMessage());
                }
                if(acteur != null) {
                    infoActeurBD();
                    button3.setEnabled(true);
                    progressBar1.setValue(acteur.getNacteur());
                }
            }
        });

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{
                    acteur = cinema.getLastActeur();
                    button3.setEnabled(false);
                }catch (Exception ex) {
                    System.err.println(ex.getMessage());
                }
                if(acteur != null) {
                    infoActeurBD();
                    button2.setEnabled(true);
                    progressBar1.setValue(acteur.getNacteur());
                }
            }
        });
    }

    private void init() throws Exception{

        button1.setIcon(new javax.swing.ImageIcon("/home/michael/Documents/boutons/first.GIF"));
        button2.setIcon(new javax.swing.ImageIcon("/home/michael/Documents/boutons/previous.GIF"));
        button3.setIcon(new javax.swing.ImageIcon("/home/michael/Documents/boutons/next.GIF"));
        button4.setIcon(new javax.swing.ImageIcon("/home/michael/Documents/boutons/last.GIF"));

        this.rootPanel.setBackground(sombre);
        this.nomLabel.setBackground(sombre);
        this.prenomLabel.setBackground(sombre);
        this.nomLabel.setForeground(Color.white);
        this.prenomLabel.setForeground(Color.white);
        this.test.setBackground(sombre);
        this.panel2.setBackground(sombre);

        this.cinema = new NavigateurActeur();
        this.progressBar1.setForeground(blue);
        this.progressBar1.setMaximum(cinema.getNbActeur());
        acteur = cinema.getFirstActeur();
        infoActeurBD();
        button2.setEnabled(false);


    }

    private void infoActeurBD(){
        try{
            nacteur = acteur.getNacteur();
            this.textField1.setText(acteur.getNom());
            this.textField2.setText(acteur.getPrenom());
        }catch (Exception sqlex){
            System.err.println(sqlex.getMessage());
        }
    }

    public static void main(String[] args) throws Exception {
        JFrame frame = new JFrame("CarriereActeur");
        frame.setContentPane(new CarriereActeur().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

package dto;

import java.io.Serializable;

public class GenreDTO implements Serializable {

    //-----------------------------attributs d'instance-----------------------------------
    private static final long serialVersionUID = 1L;
    private int ngenre ;
    private String nature;

    //------------P A S   D E   S E T T E R   C A R   L E C T U R E   S E U L -------------

    // ------------------------------- G E T T E R ------------------------------------------
    public int getNgenre() {
        return ngenre;
    }

    public String getNature() {
        return nature;
    }

    //-------------------------------C O N S T R U C T E U R --------------------------------
    public GenreDTO(int ngenre, String nature) {
        super();
        this.ngenre = ngenre;
        this.nature = nature;
    }

    //---------------------------------- T O S T R I N G -----------------------------------


    @Override
    public String toString() {
        return
                 nature
                ;
    }
}

package vues;

import modele.Personne;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;

public class MailingDlg extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextArea textAreaInfo;
    private JRadioButton nomRadioButton;
    private JRadioButton régionRadioButton;
    private JCheckBox ascendantCheckBox;
    private JButton fichierButton;
    private JButton aperçuButton;
    private JButton serialisationButton;

    private List<Personne> liste = new LinkedList<Personne>();
    private DefaultListModel<Personne> modele;

    public MailingDlg() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        //buttonOK.addActionListener(e -> onOK());   // meme fonction que celle en dessous mais simplifiée

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        aperçuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                onApercu();
            }
        });
        fichierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PrintWriter sortie = null;
                textAreaInfo.setText("");
                try{
                    JFileChooser fileChooser = new JFileChooser(".");
                    int status = fileChooser.showSaveDialog(null);

                    if (status==JFileChooser.APPROVE_OPTION){

                        File selectedFile = fileChooser.getSelectedFile();
                        sortie = new PrintWriter(new FileWriter(selectedFile));
                        trier();
                        String format = "";

                        if (nomRadioButton.isSelected()){
                            format = "Nom    : %s\nPrenom : %s\nRégion : %s\n";
                            for (Personne p : liste) {
                                sortie.println(String.format(format, p.getNom(), p.getPrenom(), p.getRegion()) + "\n");
                            }
                        } else {
                            format = "Région : %s\nNom    : %s\nPrenom : %s\n";
                            for (Personne p : liste) {
                                sortie.println(String.format(format, p.getRegion(), p.getNom(), p.getPrenom()) + "\n");
                            }
                        }
                        sortie.close();
                    }
                }
                catch (Exception ioe){
                    ioe.printStackTrace();
                }
            }
        });


        serialisationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    JFileChooser fileChooser = new JFileChooser(".");
                    int status = fileChooser.showSaveDialog(null);

                    if (status == JFileChooser.APPROVE_OPTION) {

                        File selectedFile = fileChooser.getSelectedFile();
                        FileOutputStream f = new FileOutputStream(selectedFile);
                        ObjectOutputStream out = new ObjectOutputStream(f);

                        out.writeObject(modele);
                        out.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }


    // ------------ G E S T I O N  D E  L A  C O N V E R S A T I O N -------------
    public void ouvrir(DefaultListModel<Personne> modele){
        this.modele=modele;
        this.setVisible(true);
    }

    private void onOK(){
        this.setVisible(false);
    }

    private void onApercu(){
        trier();
        textAreaInfo.setText("");
        String format = "";

        if (nomRadioButton.isSelected()) {
            format = "Nom    : %s\nPrenom : %s\nRégion : %s\n";
            for (Personne p : liste) {
                textAreaInfo.append(String.format(format, p.getNom(), p.getPrenom(), p.getRegion()) + "\n");
            }
        } else {
            format = "Région : %s\nNom    : %s\nPrenom : %s\n";
            for (Personne p : liste) {
                textAreaInfo.append(String.format(format, p.getRegion(), p.getNom(), p.getPrenom()) + "\n");
            }
        }

        textAreaInfo.select(0,0);
    }
    // ----------------------- F I N ---------------------------------------------

    // --------------------- M E T H O D E ---------------------------------------

    public void trier(){
        //Classe Auxiliaire : tri different de l'ordre naturel
        class ComparaisonRegionCroissante implements Comparator<Personne>{
            public int compare(Personne pa, Personne pb){
                int etat = pa.getRegion().compareTo(pb.getRegion());
                if (etat != 0) {
                    return etat;
                }
                return pa.getNom().compareTo(pb.getNom());
            }
        }

        class ComparaisonRegionDecroissante implements Comparator<Personne>{
            public int compare(Personne pa, Personne pb){
                int etat = pa.getRegion().compareTo(pb.getRegion());
                if (etat != 0) {
                    return -etat;
                }
                return pa.getNom().compareTo(pb.getNom());
            }
        }

        liste = new LinkedList<Personne>();
        //permet de parcourir l'enumeration de personnes
        Enumeration<Personne> iter = modele.elements();
        while(iter.hasMoreElements()){
            liste.add(iter.nextElement());
        }
        if (this.nomRadioButton.isSelected()){
            if(this.ascendantCheckBox.isSelected()){
                Collections.sort(liste);
            }else{
                Collections.sort(liste, Collections.reverseOrder());
            }
        }else if (this.ascendantCheckBox.isSelected()){
            //classe interne
            Collections.sort(liste, new ComparaisonRegionCroissante());
        }else {
            Collections.sort(liste, new ComparaisonRegionDecroissante());
        }
    }






    public static void main(String[] args) {
        MailingDlg dialog = new MailingDlg();
        dialog.pack();
        DefaultListModel<Personne> modele = new DefaultListModel<>();
        modele.addElement(new Personne("gator", "nathalie", "nord"));
        modele.addElement(new Personne("gator", "magalie", "PACA"));
        modele.addElement(new Personne("afeux", "bernard", "nord"));

        dialog.ouvrir(modele);
        //dialog.setVisible(true);
        System.exit(0); //arrete l'application
    }


}

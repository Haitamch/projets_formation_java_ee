package vues;

import modele.Personne;

import javax.swing.*;
import java.awt.event.*;

public class AjoutDlg extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel rootPanel;
    private JTextField nomTextField;
    private JLabel nomLabel;
    private JTextField prenomTextField;
    private JLabel prenomLabel;
    private JTextField regionTextField;
    private JLabel regionLabel;

    public AjoutDlg() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        this.status = true;
        this.setVisible(false);
    }

    private void onCancel() {
        // add your code here if necessary
        this.status=false;
        this.setVisible(false);
    }

    //----------------------------------------------------------------------------------
    Personne pers = null;
    boolean status = true;

    public boolean ouvrirDlg(Personne pers, boolean isAjout){
        this.pers= pers;
        if (isAjout){
            this.setTitle("Ajout d'une personne");
            this.nomTextField.setEditable(true);
            this.nomTextField.setText("");
            this.prenomTextField.setText("");
            this.regionTextField.setText("NORD");
        }
        else{
            this.setTitle("Modification de " + pers.getNom());
            this.nomTextField.setEditable(false);
            this.nomTextField.setText(pers.getNom());
            this.prenomTextField.setText(pers.getPrenom());
            this.regionTextField.setText(pers.getRegion());
        }
        //this.status=true;
        this.setVisible(true);
        if(status){
            this.pers.setNom(this.nomTextField.getText());
            this.pers.setPrenom(this.prenomTextField.getText());
            this.pers.setRegion(this.regionTextField.getText());
        }
        return status;
    }



    //-----------------------------------------------------------------------------------
    public static void main(String[] args) {
        Personne p = new Personne(null,null,null);

        //creer la boite de dialogue
        AjoutDlg dialog = new AjoutDlg();
        dialog.pack();

        //affiche la boite de dialogue
        if (dialog.ouvrirDlg(p, false)){
            System.out.println(p);
            System.out.println(p.getRegion());
        }
        System.exit(0);
    }
}

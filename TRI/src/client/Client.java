package client;

import modele.Personne;
import modele.PersonneComparator;

import java.util.*;

public class Client {
    //attribut d'instance
    private List<Personne> ens = new ArrayList<>();

    //METIER
    public void ajoutPersonne(String nom, String prenom, Double salaire){
        Personne pers = new Personne(nom, prenom, salaire);
        ens.add(pers);
    }

    public void afficherPatronymeCroissant(){
        List<Personne> aux = new LinkedList<>(this.ens);
        Collections.sort(aux);

        for (Personne p : aux){
            System.out.println(p);
        }
    }

    public void afficherPatronymeDeroissant(){
        List<Personne> aux = new LinkedList<>(this.ens);
        Collections.sort(aux,Collections.reverseOrder());

        for (Personne p : aux){
            System.out.println(p);
        }
    }

    public void afficherSalaireCroissant(){
        List<Personne> aux = new LinkedList<>(this.ens);
        Collections.sort(aux, new PersonneComparator());

        for (Personne p : aux){
            System.out.println(p);
        }
    }

    public void afficherSalaireDeroissant(){

        //----------- C L A S S E  L O C A L -----------------
        class PersonneComparatorDec implements Comparator<Personne> {
            @Override
            public int compare(Personne un, Personne deux) {
                int aux = un.getSalaire().compareTo((deux.getSalaire()));

                if (aux!=0)
                    return -aux;                                                //pour decroissant on met -
                else
                    return -un.getPatronyme().compareTo(deux.getPatronyme());   //pour decroissant on met -
            }
        }

        // -------------- L I S T E  P E R S O N N E ------------
        List<Personne> aux = new LinkedList<>(this.ens);
        Collections.sort(aux, new PersonneComparatorDec());

        for (Personne p : aux){
            System.out.println(p);
        }
    }

    public static void main(String[] args) {


    }





}

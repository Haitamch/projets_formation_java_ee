package modele;

import java.util.*;

public class Patronyme implements Comparable<Patronyme>{
    //attributs d'intance
    public String nom = "DUPOND";
    public String prenom = "JEAN";

    //getter
    public String getNom() {
        return nom;
    }
    public String getPrenom() {
        return prenom;
    }

    //setter
    public void setNom(String nom) {
        this.nom = (nom==null ? "DUPOND" : nom.toUpperCase());
    }
    public void setPrenom(String prenom) {
        this.prenom = (prenom==null ? "JEAN" : prenom.toUpperCase());
    }

    //constructeur
    public Patronyme(String nom, String prenom) {
        this.setNom(nom);
        this.setPrenom(prenom);
    }

    public Patronyme(){
        this.setNom(null);
        this.setPrenom(null);
    }

    //tostring
    @Override
    public String toString() {
        return "Patronyme{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }

    //equals hashcode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patronyme)) return false;
        Patronyme patronyme = (Patronyme) o;
        return nom.equals(patronyme.nom) &&
                prenom.equals(patronyme.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    //comparable

    @Override
    public int compareTo(Patronyme autre) {
        int aux = this.nom.compareTo(autre.nom);

        if (aux !=0)
            return aux;

        return this.prenom.compareTo(autre.prenom);
    }

    public static void main(String[] args) {
        Patronyme p1 = new Patronyme("afreux", "pierre");
        Patronyme p2 = new Patronyme("xabiste", "jean");
        Patronyme p3 = new Patronyme("gator", "nathalie");
        Patronyme p4 = new Patronyme("gator", "magalie");

        Set<Patronyme> ens = new TreeSet<>();

        ens.add(p1);
        ens.add(p2);
        ens.add(p3);
        ens.add(p4);

        System.out.println(ens);

        Map<Patronyme, Integer> ensdico = new TreeMap<>();
        ensdico.put(p1, 15);
        ensdico.put(p2, 18);
        ensdico.put(p3, 7);
        ensdico.put(p4, 56);

        System.out.println(ensdico.keySet());

        List<Integer> liste = new LinkedList<>(ensdico.values());
        Collections.sort(liste);
        System.out.println(liste);


    }
}

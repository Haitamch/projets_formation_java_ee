package modele;


import java.util.*;

import static modele.EnumNature.*;
import static modele.EnumSexe.*;

public class Personnage implements Comparable<Personnage> {
    //-------------------------attributs d'instance----------------------
    public String nom;
    public String prenom;
    public String metier;
    public EnumSexe sexe;
    public EnumNature nature;

    public Set<Album> apparait = new TreeSet<>();

    //------------------------------- Setter ----------------------------
    public void setNom(String nom) {
        this.nom = (nom==null?"NONE":nom.toUpperCase());
    }

    public void setPrenom(String prenom) {
        this.prenom = (prenom==null?"NONE":prenom.toUpperCase());
    }

    public void setMetier(String metier) {
        this.metier = (metier==null?"BRANLEUR":metier.toUpperCase());
    }

    public void setSexe(EnumSexe sexe) {
        this.sexe = (sexe==null?FEMME:sexe);
    }

    public void setNature(EnumNature nature) {
        this.nature = (nature==null?GENTIL:nature);
    }

    //--------------------------------- Getter ------------------------------
    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMetier() {
        return metier;
    }

    public EnumSexe getSexe() {
        return sexe;
    }

    public EnumNature getNature() {
        return nature;
    }

    public Set<Album> getApparait() {
        return apparait;
    }

    //---------------------------------- Constructeur ----------------------
    public Personnage(String nom, String prenom, String metier, EnumSexe sexe, EnumNature nature) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setMetier(metier);
        this.setSexe(sexe);
        this.setNature(nature);
    }

    public Personnage(String nom, String prenom) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setMetier(null);
        this.setSexe(null);
        this.setNature(null);
    }

    //---------------------------------- tostring -----------------------------
    @Override
    public String toString() {
        return "\nPersonnage\n" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", metier='" + metier + '\'' +
                ", sexe=" + sexe +
                ", nature=" + nature +
                "\n" +
                "apparait dans les albums: \n"+
                apparait
                ;
    }

    // ------------------------ equals ----------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Personnage)) return false;
        Personnage that = (Personnage) o;
        return nom.equals(that.nom) &&
                prenom.equals(that.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    // ---------------------------- Compare ----------------------------------
    @Override
    public int compareTo(Personnage autre) {
        return nom.compareTo(autre.nom);
    }

    //----------------------------------- Metier ------------------------------
    public void participe(Album a){
        a.addPerso(this);
        this.addAlbum(a);
    }

    private void addAlbum(Album a) {
        apparait.add(a);
    }


}

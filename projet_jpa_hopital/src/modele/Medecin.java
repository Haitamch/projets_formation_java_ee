package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "Medecin", uniqueConstraints = {@UniqueConstraint(columnNames = {"nom", "prenom"})})
public class Medecin implements Serializable {
    private static final long serialVersion = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int id;
    @Column(nullable = false, length = 30)
    private String nom;
    @Column(nullable = false, length = 30)
    private String prenom;

    private float salaire;

    @ManyToOne
    @JoinColumn(name="service_id")
    private Service service;

    @ManyToOne
    @JoinColumn(name = "manager_id")
    private Medecin mgr;

    @OneToMany(mappedBy = "mgr")
    private Set<Medecin> subs = new HashSet<>();

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public float getSalaire() {
        return salaire;
    }

    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    protected Medecin(){
        this.nom="Dupont";
        this.prenom="Bernard";
    }

    public Medecin(String nom, String prenom, float salaire){
        this.nom= nom.toUpperCase();
        this.prenom= prenom;
        this.salaire=salaire;
    }

    public Medecin getMgr() {
        return mgr;
    }

    public Set<Medecin> getSubs() {
        return subs;
    }

    public void setSubs(Set<Medecin> subs) {
        this.subs = subs;
    }

    public void setMgr(Medecin mgr){
        if (this.getMgr() !=null ){
            if (this.getMgr().equals(mgr))
                return;
            else
                this.getMgr().getSubs().remove(this);
        }
        //gestion des deux extremites
        if (mgr!=null) {
            mgr.subs.add(this);
        }
        this.mgr = mgr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Medecin)) return false;
        Medecin medecin = (Medecin) o;
        return nom.equals(medecin.nom) &&
                prenom.equals(medecin.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    @Override
    public String toString() {
        return "Medecin{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", salaire=" + salaire +
                ", service=" + (service==null?", sans service" : service.getNom()) +
                ", mgr=" + (mgr==null?"sans manager":mgr) +
                '}';
    }
}

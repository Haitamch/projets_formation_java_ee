package dto;

import java.io.Serializable;
import java.sql.Date;

public class CommandeDTO implements Serializable {
    private long nCommande;
    private Date dateCommande;
    private String nomClient;

    //GETTER SETTER
    public long getnCommande() {
        return nCommande;
    }

    public void setnCommande(long nCommande) {
        this.nCommande = nCommande;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    //constructeur
    public CommandeDTO(long nCommande, Date dateCommande, String nomClient) {
        this.nCommande = nCommande;
        this.dateCommande = dateCommande;
        this.nomClient = nomClient;
    }

    //tostring
    @Override
    public String toString() {
        return "CommandeDTO{" +
                "nCommande=" + nCommande +
                ", dateCommande=" + dateCommande +
                ", nomClient='" + nomClient + '\'' +
                '}';
    }

}

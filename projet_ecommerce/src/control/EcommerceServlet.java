package control;

import dto.Catalogue;
import dto.ClientDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static dao.EcommerceDAO.*;

@WebServlet("/process")
public class EcommerceServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request, response);
    }

    //navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }

    //routage
    private void processRquest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            //recupertaion de l'action
            String action = request.getParameter("action");

            //execution du traitement
            if (action.equals("init")){
                doInit(request, response);
            }else if (action.equals("authenticate")){
                doAuthenticate(request, response);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void doAuthenticate(HttpServletRequest request, HttpServletResponse response) {
        try {
            String nom = request.getParameter("nom");
            String motDePasse = request.getParameter("motDePasse");

            //1 recherche du client
            ClientDTO client = INSTANCE.findClientsByNom(nom);
            if (client != null){
                if (client.getPass().equals(motDePasse)){
                    request.getSession().setAttribute("client", client);
                }else {
                    request.getSession().removeAttribute("client");
                    client=null;
                }
            }

            // cinematique
            String url = (client == null ? "pages/showLogin.jsp":"pages/showCatalogue.jsp");
            forward(url, request, response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException , IOException {
        //gestion du cache application
        if (this.getServletContext().getAttribute("catalogue")==null){
            Catalogue catalogue = INSTANCE.getCatalogue();

            this.getServletContext().setAttribute("catalogue", catalogue);
        }
        String url = "pages/showLogin.jsp";
        forward(url, request, response);

    }
}

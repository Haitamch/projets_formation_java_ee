<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 25/02/2020
  Time: 17:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Catalogue</title>
</head>
<body>
<form method="post" action="process?action=addItems">
    <table align="center">
        <thead>
                <th>id</th>
                <th>descriptif</th>
                <th>prix</th>
                <th>disponible</th>
                <th>stock</th>
        </thead>
        <tbody>
        <c:forEach items="${catalogue.produits}" var="myitem">
            <tr>
                <td> <input type="checkbox" name="itemId" value="${myitem.value.nProd}">
                </td>
                <td>${myitem.value.descript}</td>
                <td>${myitem.value.prix}</td>
                <td>${myitem.value.dispo}</td>
                <td>${myitem.value.stock}</td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="5">
                <input type="submit" value="envoie">
            </td>
        </tr>
        </tbody>
    </table>

</form>


</body>
</html>

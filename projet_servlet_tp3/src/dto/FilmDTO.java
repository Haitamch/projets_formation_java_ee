package dto;

import java.io.Serializable;

public class FilmDTO implements Serializable {

    // ---------------------attributs d'instance-------------------------
    private static final long serialVersionUID = 1L;

    private int nfilm ;
    private String titre;
    private String nomRealisateur;
    private String ngenre;
    private int entrees;
    private String sortie;
    private String pays;

    // ---------------------------- G E T T E R --------------------------

    public int getNfilm() {
        return nfilm;
    }

    public void setNfilm(int nfilm) {
        this.nfilm = nfilm;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getNomRealisateur() {
        return nomRealisateur;
    }

    public void setNomRealisateur(String nomRealisateur) {
        this.nomRealisateur = nomRealisateur;
    }

    public String getNgenre() {
        return ngenre;
    }

    public void setNgenre(String ngenre) {
        this.ngenre = ngenre;
    }

    public int getEntrees() {
        return entrees;
    }

    public void setEntrees(int entrees) {
        this.entrees = entrees;
    }

    public String getSortie() {
        return sortie;
    }

    public void setSortie(String sortie) {
        this.sortie = sortie;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }


    // ----------------------- C O N S T R U C T E U R ---------------------------------

    public FilmDTO(int nfilm, String titre, String nomRealisateur, String ngenre, int entrees, String sortie, String pays) {
        this.nfilm = nfilm;
        this.titre = titre;
        this.nomRealisateur = nomRealisateur;
        this.ngenre = ngenre;
        this.entrees = entrees;
        this.sortie = sortie;
        this.pays = pays;
    }

    public FilmDTO(int nfilm, String titre, String nomRealisateur, String ngenre) {
        this.nfilm = nfilm;
        this.titre = titre;
        this.nomRealisateur = nomRealisateur;
        this.ngenre = ngenre;
    }
//--------------------------- T O S T R I N G ---------------------------------------


    @Override
    public String toString() {
        return "FilmDTO{" +
                "nfilm=" + nfilm +
                ", titre='" + titre + '\'' +
                ", nomRealisateur='" + nomRealisateur + '\'' +
                ", ngenre='" + ngenre + '\'' +
                ", entrees=" + entrees +
                ", sortie='" + sortie + '\'' +
                ", pays='" + pays + '\'' +
                '}';
    }
}

package modele;

import java.util.*;

public class TintinMania {
    //------------------ Attributs d'instance ------------------
    private List<Album> mesTintins = new ArrayList<Album>();

    //------------------ getter --------------------------------
    public List<Album> getMesTintins() {
        return mesTintins;
    }

    //----------------------- tostring --------------------------
    @Override
    public String toString() {
        return "TintinMania :\n" +
                "mesTintins = \n" + mesTintins +
                "";
    }

    //----------------------- METIER ---------------------------------
    public void chargerAlbums(){
        Album a1 = new Album("Tintin au pays des Soviets", 1930);
        Album a2 = new Album("Tintin au Congo", 1931);
        Album a3 = new Album("Tintin en Amérique", 1932);
        Album a4 = new Album("Les Cigares du pharaon", 1934);
        Album a5 = new Album("Le Lotus bleu", 1936);
        Album a6 = new Album("L'Oreille cassée", 1937);
        Album a7 = new Album("L'Ile noire", 1938);
        Album a8 = new Album("Le Sceptre d'Ottokar", 1939);
        Album a9 = new Album("Le Crabe aux pinces d'or", 1941);
        Album a10= new Album("L'Étoile mystérieuse", 1942);
        Album a11= new Album("Le Secret de la Licorne", 1943);
        Album a12= new Album("Le Trésor de Rackham le Rouge", 1944);
        Album a13= new Album("Les 7 boules de cristal", 1948);
        Album a14= new Album("Le Temple du soleil", 1950);

        mesTintins.add(a1);
        mesTintins.add(a2);
        mesTintins.add(a3);
        mesTintins.add(a4);
        mesTintins.add(a5);
        mesTintins.add(a6);
        mesTintins.add(a7);
        mesTintins.add(a8);
        mesTintins.add(a9);
        mesTintins.add(a10);
        mesTintins.add(a11);
        mesTintins.add(a12);
        mesTintins.add(a13);
        mesTintins.add(a14);
    }

    public void listeAlbumsParTitre(){
    List<Album> liste1 = new LinkedList<>(this.mesTintins);
        Collections.sort(liste1);

        for (Album a : liste1){
            System.out.println(a);
        }
    }

    public void listeAlbumsParTitreDec(){
        List<Album> liste1 = new LinkedList<>(this.mesTintins);
        Collections.sort(liste1,Collections.reverseOrder());

        for (Album a : liste1){
            System.out.println(a);
        }
    }

    public void listeAlbumsParAnnee(){
        //---------------------- C L A S S E  L O C A L ------------------
        class AlbumComparator implements Comparator<Album> {
            @Override
            public int compare(Album a1, Album a2) {
                Integer annee = a1.getAnnee().compareTo(a2.getAnnee());

                if(annee != 0 )
                    return annee;
                return a1.getTitre().compareTo(a2.getTitre());
            }
        }
        List<Album> liste1 = new LinkedList<>(this.mesTintins);
        Collections.sort(liste1,new AlbumComparator());

        for (Album a : liste1){
            System.out.println(a);
        }
    }

    public void listeAlbumsParAnneeDec(){
        //-------------------- C L A S S E  A N O N Y M E ---------------------
        /*Comparator<Album> comp  = new Comparator<Album>() {
            public int compare(Album a1, Album a2) {
                Integer annee = a1.getAnnee().compareTo(a2.getAnnee());

                if (annee != 0)
                    return -annee;
                return -a1.getTitre().compareTo(a2.getTitre());
            }
        };*/
        List<Album> liste1 = new LinkedList<>(this.mesTintins);
        Collections.sort(liste1,
                // ----- C L A S S E  A N O N Y M E ------
                new Comparator<Album>() {
            public int compare(Album a1, Album a2) {
                Integer annee = a1.getAnnee().compareTo(a2.getAnnee());

                if (annee != 0)
                    return -annee;
                return -a1.getTitre().compareTo(a2.getTitre());
            }
        });

        for (Album a : liste1){
            System.out.println(a);
        }
    }

}
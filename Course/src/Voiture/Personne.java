package Voiture;

public abstract class Personne {
    //variable d'instance
    private int matricule;
    private String nom;
    private String prenom;

    //association
    public Voiture voiture = null; //je suis pieton

    //variable static
    private static int dernierMatricule = 1;

    //setter
    private void setMatricule() {
        this.matricule = Personne.getDernierMatricule();
        Personne.setDernierMatricule();
    }

    public void setNom(String nom) {
        this.nom = (nom==null || nom.trim().equals("") ? "INCONNU" : nom.toUpperCase());
    }

    public void setPrenom(String prenom) {
        this.prenom = (prenom==null || prenom.trim().equals("") ? "INCONNU": prenom.toUpperCase());
    }

    private static void setDernierMatricule() {
        Personne.dernierMatricule++;
    }

    //getter

    public int getMatricule() {
        return matricule;
    }

    public String getNom() {
        return nom;
    }

    private String getPrenom() {
        return prenom;
    }

    private static int getDernierMatricule() {
        return dernierMatricule;
    }

    // metier

    public boolean estPieton(){
        return (this.voiture == null);
    }

    public Voiture getVoiture() {
        return voiture;
    }

    public void setVoiture(Voiture voiture) {
        this.voiture = voiture;
    }

    protected abstract boolean estCompatible(Voiture v);


    public void affecter(Voiture v){


        //cas 0 : La voiture doit exister
        if(v == null) return;

        //cas 1
        if(! this.estCompatible(v)) return;

        //cas 2 : la voiture n'est pas disponible
        if(! v.estDisponible()) return;

        //cas 3 : je ne suis pas pieton
        if (! this.estPieton()) return;

        //action 1 La voiture a un conducteur
        v.setConducteur(this); //Moi

        //action 2 je sui conducteur de v
        this.setVoiture(v);
    }

    public void restituer(){
        //cas 1 : je dois etre conducteur
        if(this.estPieton()) return;
        //la voiture perd sont conducteur
        this.getVoiture().setConducteur(null);
        //je suis pieton
        this.setVoiture(null);
    }




//constructeur

    public Personne(String nom, String prenom) {
        this.setMatricule();
        this.setNom(nom);
        this.setPrenom(prenom);
    }

    protected Personne() {
        this(null,null);
    }

    //string


    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "matricule=" + matricule +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", pilote" + (voiture==null ? " aucune voiture" : " une " + voiture.getMarque()) +
                '}';
    }



    public static void main(String[] args) {
       /* Personne p1 = new Personne("Lefebvre", "Michael");
        Personne p2 = new Personne("Bouchez", " Marc");
        Personne inconnu = new Personne();
        Voiture v1 = new Voiture("REnault", 2000, 'E');
        Voiture v2 = new Voiture("Nissan", 1000, 'D');
        p1.affecter(v1);
        p2.affecter(v2);
        System.out.println("le conducteur P1 :");
        System.out.println(p1);
        System.out.println("le conducteur P2 :");
        System.out.println(p2);
        System.out.println("le conducteur inconnu :");
        System.out.println(inconnu);
        p1.restituer();
        p2.restituer();
        System.out.println("le conducteur P1 :");
        System.out.println(p1);
        System.out.println("le conducteur P2 :");
        System.out.println(p2);
        System.out.println(v1);*/

    }
}

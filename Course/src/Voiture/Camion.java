package Voiture;

public class Camion extends Voiture {

    //attributs d'instance
    private int tonnage;

    //Getter setter

    public int getTonnage() {
        return tonnage;
    }

    private void setTonnage(int tonnage) {
        this.tonnage = (tonnage <= 5 ? 5 : tonnage);
    }

    //contructeur


    public Camion(String marque, int puissance, char carburant, int tonnage) {
        super(marque, puissance, carburant);
        this.setTonnage(tonnage);
    }

    public Camion(int tonnage) {
        super();
        this.tonnage = 7;
    }

    public Camion(String marque, Moteur moteur, int tonnage) {
        super(marque, moteur);
        this.setTonnage(tonnage);
    }

    @Override
    public String toString() {
        return super.toString() + "\n\t\t" + "{" +
                "tonnage=" + tonnage +
                '}';
    }

    public static void main(String[] args) {
        Camion c1 = new Camion("volvo", 5000, 'D', 6);
        System.out.println(c1);
    }

}
